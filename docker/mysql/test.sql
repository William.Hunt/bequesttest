Use
test_db;

Create table `tbl_answers`
(
    id           varchar(200),
    value        varchar(200),
    created_date date,
    created_by   varchar(200)
);

-- Insert into test_tb (id, value)
-- values ('key', 'test-user', now(), 'testuser');