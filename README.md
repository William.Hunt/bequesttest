# Bequest Test

A simple golang API for Bequest Application



## Getting Started

### Dependencies

* Docker compose

### Installing

* Simply run docker-compose up --build in the main directory

### Executing program

* Once docker compose has completed then you need to go to localhost/CreateTable to initialise the database table. This can also be used to delete any test data and start again.
* Once the table has been created, all you need to do is access any of the URL's stored in the postman collection.
