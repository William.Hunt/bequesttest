package main

import (
	_ "bequest-test/general/handlers"
	"bequest-test/general/router"
	"flag"
	"github.com/gorilla/handlers"
	"net/http"
	"os"
	"runtime"
)

func main() {
	portPtr := flag.String("port", "25567", "The port to run the server on.")
	flag.Parse()
	go InitialiseHttpListener(portPtr)
	runtime.Goexit()
}

func InitialiseHttpListener(port *string) {
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "origin", "content-type", "Authorization", "authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	http.ListenAndServe(":"+*port, handlers.LoggingHandler(os.Stdout, handlers.CORS(originsOk, headersOk, methodsOk)(router.NewRouter())))
}
