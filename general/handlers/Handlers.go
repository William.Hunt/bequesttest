package handlers

import (
	"bequest-test/general/database"
	"bequest-test/general/router"
	"fmt"
	"net/http"
)

func init() {
	router.AddDef(router.Route{Method: "GET", Pattern: "/CreateTable", HandlerFunc: CreateTable})
	router.AddDef(router.Route{Method: "GET", Pattern: "/GetHistory/{key}", HandlerFunc: GetHistory})
	router.AddDef(router.Route{Method: "GET", Pattern: "/Answer/{key}", HandlerFunc: GetAnswer})
	router.AddDef(router.Route{Method: "POST", Pattern: "/Answer", HandlerFunc: CreateAnswer})
	router.AddDef(router.Route{Method: "PUT", Pattern: "/Answer", HandlerFunc: UpdateAnswer})
	router.AddDef(router.Route{Method: "DELETE", Pattern: "/Answer/{key}", HandlerFunc: DeleteAnswer})
}
func CreateTable(w http.ResponseWriter, r *http.Request) {
	tx, _, _, _ := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	database.RunDataChange(`drop table tbl_answers`, tx)
	database.RunDataChange(`Create table tbl_answers
(
    id           varchar(200),
    value        varchar(200),
    created_date datetime,
    created_by   varchar(200),
    event_type varchar(200)
)`, tx)
	tx.Commit()
	fmt.Fprintln(w, "table created successfully")
}
func GetHistory(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	fmt.Fprintln(w, database.RunGet("select id,value,created_date,event_type from tbl_answers where id = ?  order by created_date desc", database.MySql, data["key"]))

}
func GetAnswer(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	fmt.Fprintln(w, database.RunGet(`
select ans.id, value
from tbl_answers ans
         INNER JOIN (
    SELECT id, MAX(created_date) AS max_Date
    FROM tbl_answers
    GROUP BY id
) ms ON ans.id = ms.id AND created_date = max_Date
where ans.id = ?
  and event_type != 'delete'
`, database.MySql, data["key"]))

}
func CreateAnswer(w http.ResponseWriter, r *http.Request) {
	tx, _, postData, params := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	if len(database.GetQueryAsArray(`select ans.id
from tbl_answers ans
         INNER JOIN (
    SELECT id, MAX(created_date) AS max_Date
    FROM tbl_answers
    GROUP BY id
) ms ON ans.id = ms.id AND created_date = max_Date
where ans.id = ?
  and event_type != 'delete'`, database.MySql, postData["key"])) > 0 {
		fmt.Fprintln(w, "record allready exists")
		return
	}
	sql := `insert into
  tbl_answers (
    id,
    value,
    created_date,
    created_by,
event_type
  )
values
  (?, ?, now(), 'noauth','create')`
	params = append(params, postData["key"], postData["value"])
	database.RunDataChange(sql, tx, params...)
	tx.Commit()
	fmt.Fprintln(w, "successfully added record")
}
func UpdateAnswer(w http.ResponseWriter, r *http.Request) {
	tx, _, postData, params := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	if recordDeleted(postData["key"]) {
		fmt.Fprintln(w, "record deleted")
		return
	}
	if len(database.GetQueryAsArray(`select id from tbl_answers where id = ? and event_type = 'create'`, database.MySql, postData["key"])) == 0 {
		fmt.Fprintln(w, "no record exists")
		return
	}
	sql := `insert into
  tbl_answers (
    id,
    value,
    created_date,
    created_by,
event_type
  )
values
  (?, ?, now(), 'noauth','update')`
	params = append(params, postData["key"], postData["value"])
	database.RunDataChange(sql, tx, params...)
	tx.Commit()
	fmt.Fprintln(w, "successfully updated record")
}
func DeleteAnswer(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	tx, _, _, params := database.GetPostData(r, database.MySql)
	defer tx.Rollback()
	if recordDeleted(data["key"]) {
		fmt.Fprintln(w, "record already deleted")
		return
	}
	if len(database.GetQueryAsArray(`select id from tbl_answers where id = ? and event_type = 'create'`, database.MySql, data["key"])) == 0 {
		fmt.Fprintln(w, "no record exists")
		return
	}
	lastRecord := database.GetQueryAsArray(`
select ans.id, value
from tbl_answers ans
         INNER JOIN (
    SELECT id, MAX(created_date) AS max_Date
    FROM tbl_answers
    GROUP BY id
) ms ON ans.id = ms.id AND created_date = max_Date
where ans.id = ?
  and event_type != 'delete'
`, database.MySql, data["key"])
	sql := `insert into
  tbl_answers (
    id,
    value,
    created_date,
    created_by,
event_type
  )
values
  (?, ?, now(), 'noauth','delete')`
	params = append(params, data["key"], lastRecord[0]["value"])
	database.RunDataChange(sql, tx, params...)
	tx.Commit()
	fmt.Fprintln(w, "successfully deleted record")
}

func recordDeleted(key interface{}) bool {
	return len(database.GetQueryAsArray(`select ans.id
from tbl_answers ans
         INNER JOIN (
    SELECT id, MAX(created_date) AS max_Date
    FROM tbl_answers
    GROUP BY id
) ms ON ans.id = ms.id AND created_date = max_Date
where ans.id = ?
  and event_type = 'delete'`, database.MySql, key)) > 0
}
